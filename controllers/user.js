import model from '../models';

const { User } = model;

class Users {
  static signUp(req, res) {
    const { name, username, email, password } = req.body
      return User
        .create({
          name,
          username,
          email,
          password
        })
        .then(userData => res.status(201).send({
          success: true,
          message: 'User successfully created',
          userData
        }))
        .catch(error => res.status(400).send(error));
    }
  
  static listAll(req, res) {
    return User.findAll().then(data => res.status(200).send({
      status: true, result: data, error: null
    }))
  }

  static listShow(req, res) {
    const {user_id} = req.params
    return User.findByPk(user_id).then(data => res.status(200).send({
      status: true, result: data, error: null
    }))
    .catch(error => res.status(400).send(error));
  }

  static update(req, res) {
    const { name, username, email, password } = req.body
      return User.findByPk(req.params.user_id)
        .then((data) => {data.update({
            name: name || data.name,
            username: username || data.username,
            email: email || data.email,
            password: password || data.password
          }).then((data) => {
            res.status(200).send({
              status: true, result: data, error: null
            })
          })
          .catch(error => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
  }

  static delete(req, res) {
    return User
      .findByPk(req.params.user_id)
      .then(data => {
        if(!data) {
          return res.status(400).send({
            status: true, result: null, error: "Data Not Found"
          });
        }
        return data
          .destroy()
          .then(() => res.status(200).send({
            status: true, result: "Data Success Deleted", error: null
          }))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error))
  }
}

export default Users;
