import model from '../models';

const {Book} = model;

class Books {
  static create(req, res) {
    const {title, author, description, quantity} = req.body
    const {user_id} = req.params
    return Book.create({title, author, description, quantity, userId: user_id}).then(data => res.status(201).send({
      status: true, result: data, error: null
    }))
    .catch(error => res.status(400).send(error));
  }

  static listAll(req, res) {
    const {book_id} = req.query
    return Book.sequelize.query('SELECT * FROM public."Users"', {type: Book.sequelize.QueryTypes.SELECT}).then(data => res.status(200).send({
      status: true, result: data, error: null
    }))
  }

  static listShow(req, res) {
    const {book_id} = req.params
    return Book.findByPk(book_id).then(data => res.status(200).send({
      status: true, result: data, error: null
    }))
    .catch(error => res.status(400).send(error));
  }

  static update(req, res) {
    const { title, author, description, quantity } = req.body
      return Book.findByPk(req.params.book_id)
        .then((data) => {data.update({
            title: title || data.title,
            author: author || data.author,
            description: description || data.description,
            quantity: quantity || data.quantity
          }).then((data) => {
            res.status(200).send({
              status: true, result: data, error: null
            })
          })
          .catch(error => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
  }

  static delete(req, res) {
    return Book
      .findByPk(req.params.book_id)
      .then(data => {
        if(!data) {
          return res.status(400).send({
            status: true, result: null, error: "Data Not Found"
          });
        }
        return data
          .destroy()
          .then(() => res.status(200).send({
            status: true, result: "Data Success Deleted", error: null
          }))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error))
  }
}

export default Books