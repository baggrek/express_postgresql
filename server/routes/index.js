import Users from '../../controllers/user';
import Books from '../../controllers/book';

export default (app) => {

  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the BookStore API!',
  }));

  app.get('/api/users', Users.listAll);
  app.get('/api/users/:user_id', Users.listShow);
  app.put('/api/users/:user_id', Users.update);
  app.post('/api/users', Users.signUp); // API route for user to signup
  app.delete('/api/users/:user_id', Users.delete);

  app.post('/api/users/:user_id/books', Books.create);
  app.get('/api/books/', Books.listAll);
  app.get('/api/books/:book_id', Books.listShow);
  app.put('/api/books/:book_id', Books.update);
  app.delete('/api/books/:book_id', Books.delete);
};